﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra;
using WeatherStation.QueryEntities;

namespace WeatherStation
{
    public static class DataProvider
    {
        #region CityWeatherStation
        public static CityWeatherStation GetCityWeatherStation(string city_id)
        {
            ISession session = SessionManager.GetSession();

            CityWeatherStation ws = new CityWeatherStation();

            if (session == null)
                return null;
            //promenice se city id sa parametrom
            Row CityWeatherStationData = session.Execute("select * from \"city_weather_station\" where \"city_id\"= '" + city_id + "'").FirstOrDefault();

            if (CityWeatherStationData != null)
            {
                ws.cityID = CityWeatherStationData["city_id"] != null ? CityWeatherStationData["city_id"].ToString() : string.Empty;
                ws.zipCode = CityWeatherStationData["zip_code"] != null ? CityWeatherStationData["zip_code"].ToString() : string.Empty;
                ws.country = CityWeatherStationData["country"] != null ? CityWeatherStationData["country"].ToString() : string.Empty;
            }

            return ws;
        }

        public static List<CityWeatherStation> GetCityWeatherStations()
        {
            ISession session = SessionManager.GetSession();
            List<CityWeatherStation> stations = new List<CityWeatherStation>();


            if (session == null)
                return null;

            var CityWeatherStationsData = session.Execute("select * from \"city_weather_station\"");


            foreach (var CityWeatherStationData in CityWeatherStationsData)
            {
                CityWeatherStation CityWeatherStation = new CityWeatherStation();
                CityWeatherStation.cityID = CityWeatherStationData["city_id"] != null ? CityWeatherStationData["city_id"].ToString() : string.Empty;
                CityWeatherStation.zipCode = CityWeatherStationData["zip_code"] != null ? CityWeatherStationData["zip_code"].ToString() : string.Empty;
                CityWeatherStation.country = CityWeatherStationData["country"] != null ? CityWeatherStationData["country"].ToString() : string.Empty;
                stations.Add(CityWeatherStation);
            }
            return stations;
        }

        public static void AddCityWeatherStation(string city_id, string zip, string country)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet CityWeatherStationData = session.Execute("insert into \"city_weather_station\" (\"city_id\", \"zip_code\", \"country\")  values ('" + city_id + "' , '" + zip + "' , '" + country + "')");

        }

        public static void DeleteCityWeatherStation(string city_id)
        {
            ISession session = SessionManager.GetSession();
            CityWeatherStation CityWeatherStation = new CityWeatherStation();

            if (session == null)
                return;

            RowSet CityWeatherStationData = session.Execute("delete from \"city_weather_station\" where \"city_id\" = '" + city_id + "'");

        }

        #endregion

        #region TemperatureByDay
        public static TemperatureByDay GetTemperatureByEventTime(string city_id, string date, string eventTime)
        {
            ISession session = SessionManager.GetSession();

            TemperatureByDay temp = new TemperatureByDay();

            if (session == null)
                return null;

            Row TemperatureData = session.Execute("select * from \"temperature_by_day\" where \"city_id\"='" + city_id + "' and \"date\"='" + date + "' and \"event_time\"='" + eventTime + "'").FirstOrDefault();

            if (TemperatureData != null)
            {
                temp.cityID = TemperatureData["city_id"] != null ? TemperatureData["city_id"].ToString() : string.Empty;
                temp.date = TemperatureData["date"] != null ? DateTime.Parse(TemperatureData["date"].ToString()) : DateTime.Parse(string.Empty);
                temp.eventTime = TemperatureData["event_time"] != null ? DateTime.Parse(TemperatureData["event_time"].ToString()) : DateTime.Parse(string.Empty);
                temp.temperature = TemperatureData["temperature"] != null ? Decimal.Parse(TemperatureData["temperature"].ToString()) : Decimal.Parse(string.Empty);
            }

            return temp;
        }

        public static List<TemperatureByDay> GetTemperatureByDay(string city_id, string date)
        {
            ISession session = SessionManager.GetSession();

            List<TemperatureByDay> temperatures = new List<TemperatureByDay>();


            if (session == null)
                return null;

            var TemperatureData = session.Execute("select * from \"temperature_by_day\" where \"city_id\"='" + city_id + "' and \"date\"='" + date + "'");


            foreach (var hd in TemperatureData)
            {
                TemperatureByDay h = new TemperatureByDay();
                h.cityID = hd["city_id"] != null ? hd["city_id"].ToString() : string.Empty;
                h.date = hd["date"] != null ? DateTime.Parse(hd["date"].ToString()) : DateTime.Parse(string.Empty);
                h.eventTime = hd["event_time"] != null ? DateTime.Parse(hd["event_time"].ToString()) : DateTime.Parse(string.Empty);
                h.temperature = hd["temperature"] != null ? Decimal.Parse(hd["temperature"].ToString()) : Decimal.Parse(string.Empty);
                temperatures.Add(h);
            }

            return temperatures;

        }

        public static List<TemperatureByDay> GetTemperature()
        {
            ISession session = SessionManager.GetSession();
            List<TemperatureByDay> temp = new List<TemperatureByDay>();


            if (session == null)
                return null;

            var TemperatureData = session.Execute("select * from \"temperature_by_day\"");


            foreach (var hd in TemperatureData)
            {
                TemperatureByDay h = new TemperatureByDay();
                h.cityID = hd["city_id"] != null ? hd["city_id"].ToString() : string.Empty;
                h.date = hd["date"] != null ? DateTime.Parse(hd["date"].ToString()) : DateTime.Parse(string.Empty);
                h.eventTime = hd["event_time"] != null ? DateTime.Parse(hd["event_time"].ToString()) : DateTime.Parse(string.Empty);
                h.temperature = hd["temperature"] != null ? Decimal.Parse(hd["temperature"].ToString()) : Decimal.Parse(string.Empty);
                temp.Add(h);
            }
            return temp;
        }

        public static void AddTemperature(string city_id, string date, string eventTime, string temperature)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
            try
            {
                RowSet TemperatureData = session.Execute("insert into \"temperature_by_day\" (\"city_id\", \"date\", \"event_time\", \"temperature\")  values ('" + city_id + "' , '" + date + "' , '" + eventTime + "' , '" + temperature + "')");
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void DeleteTemperatureByEventTime(string city_id, string date, string eventTime)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return;

            RowSet TemperatureData = session.Execute("delete from \"temperature_by_day\" where \"city_id\" = '" + city_id + "' and \"date\" = '" + date + "' and \"event_time\" = '" + eventTime + "'");
        }
        #endregion

        #region PressureByDay
        public static PressureByDay GetPressureByEventTime(string city_id, string date, string eventTime)
        {
            ISession session = SessionManager.GetSession();

            PressureByDay p = new PressureByDay();

            if (session == null)
                return null;

            Row PressureData = session.Execute("select * from \"pressure_by_day\" where \"city_id\"='" + city_id + "' and \"date\"='" + date + "' and \"event_time\"='" + eventTime + "'").FirstOrDefault();

            if (PressureData != null)
            {
                p.cityID = PressureData["city_id"] != null ? PressureData["city_id"].ToString() : string.Empty;
                p.date = PressureData["date"] != null ? DateTime.Parse(PressureData["date"].ToString()) : DateTime.Parse(string.Empty);
                p.eventTime = PressureData["event_time"] != null ? DateTime.Parse(PressureData["event_time"].ToString()) : DateTime.Parse(string.Empty);
                p.pressure = PressureData["pressure"] != null ? Decimal.Parse(PressureData["pressure"].ToString()) : Decimal.Parse(string.Empty);

            }

            return p;
        }

        public static List<PressureByDay> GetPressureByDay(string city_id, string date)
        {
            ISession session = SessionManager.GetSession();

            List<PressureByDay> pressures = new List<PressureByDay>();


            if (session == null)
                return null;

            var PressureData = session.Execute("select * from \"pressure_by_day\" where \"city_id\"='" + city_id + "' and \"date\"='" + date + "'");


            foreach (var hd in PressureData)
            {
                PressureByDay h = new PressureByDay();
                h.cityID = hd["city_id"] != null ? hd["city_id"].ToString() : string.Empty;
                h.date = hd["date"] != null ? DateTime.Parse(hd["date"].ToString()) : DateTime.Parse(string.Empty);
                h.eventTime = hd["event_time"] != null ? DateTime.Parse(hd["event_time"].ToString()) : DateTime.Parse(string.Empty);
                h.pressure = hd["pressure"] != null ? Decimal.Parse(hd["pressure"].ToString()) : Decimal.Parse(string.Empty);
                pressures.Add(h);
            }

            return pressures;
        }

        public static List<PressureByDay> GetPressure()
        {
            ISession session = SessionManager.GetSession();
            List<PressureByDay> pressure = new List<PressureByDay>();


            if (session == null)
                return null;

            var PressureData = session.Execute("select * from \"pressure_by_day\"");


            foreach (var hd in PressureData)
            {
                PressureByDay h = new PressureByDay();
                h.cityID = hd["city_id"] != null ? hd["city_id"].ToString() : string.Empty;
                h.date = hd["date"] != null ? DateTime.Parse(hd["date"].ToString()) : DateTime.Parse(string.Empty);
                h.eventTime = hd["event_time"] != null ? DateTime.Parse(hd["event_time"].ToString()) : DateTime.Parse(string.Empty);
                h.pressure = hd["pressure"] != null ? Decimal.Parse(hd["pressure"].ToString()) : Decimal.Parse(string.Empty);
                pressure.Add(h);
            }
            return pressure;
        }

        public static void AddPressure(string city_id, string date, string eventTime, string pressure)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
            try
            {
                RowSet PressureData = session.Execute("insert into \"pressure_by_day\" (\"city_id\", \"date\", \"event_time\", \"pressure\")  values ('" + city_id + "' , '" + date + "' , '" + eventTime + "' , '" + pressure + "')");
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void DeletePressureByEventTime(string city_id, string date, string eventTime)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet PressureData = session.Execute("delete from \"pressure_by_day\" where \"city_id\" = '" + city_id + "' and \"date\" = '" + date + "' and \"event_time\" = '" + eventTime + "'");
        }

        #endregion

        #region AirHumidityByDay
        public static AirHumidityByDay GetAirHumidityByEventTime(string city_id, string date, string eventTime)
        {
            ISession session = SessionManager.GetSession();

            AirHumidityByDay h = new AirHumidityByDay();

            if (session == null)
                return null;

            Row AirHumidityData = session.Execute("select * from \"air_humidity_by_day\" where \"city_id\"='" + city_id + "' and \"date\"='" + date + "' and \"event_time\"='" + eventTime + "'").FirstOrDefault();

            if (AirHumidityData != null)
            {
                h.cityID = AirHumidityData["city_id"] != null ? AirHumidityData["city_id"].ToString() : string.Empty;
                h.date = AirHumidityData["date"] != null ? DateTime.Parse(AirHumidityData["date"].ToString()) : DateTime.Parse(string.Empty);
                h.eventTime = AirHumidityData["event_time"] != null ? DateTime.Parse(AirHumidityData["event_time"].ToString()) : DateTime.Parse(string.Empty);
                h.airHumidity = AirHumidityData["air_humidity"] != null ? Decimal.Parse(AirHumidityData["air_humidity"].ToString()) : Decimal.Parse(string.Empty);

            }

            return h;
        }

        public static List<AirHumidityByDay> GetAirHumidityByDay(string city_id, string date)
        {
            ISession session = SessionManager.GetSession();

            List<AirHumidityByDay> humidity = new List<AirHumidityByDay>();


            if (session == null)
                return null;

            var AirHumidityData = session.Execute("select * from \"air_humidity_by_day\" where \"city_id\"='" + city_id + "' and \"date\"='" + date + "'");


            foreach (var hd in AirHumidityData)
            {
                AirHumidityByDay h = new AirHumidityByDay();
                h.cityID = hd["city_id"] != null ? hd["city_id"].ToString() : string.Empty;
                h.date = hd["date"] != null ? DateTime.Parse(hd["date"].ToString()) : DateTime.Parse(string.Empty);
                h.eventTime = hd["event_time"] != null ? DateTime.Parse(hd["event_time"].ToString()) : DateTime.Parse(string.Empty);
                h.airHumidity = hd["air_humidity"] != null ? Decimal.Parse(hd["air_humidity"].ToString()) : Decimal.Parse(string.Empty);
                humidity.Add(h);
            }

            return humidity;

        }


        public static List<AirHumidityByDay> GetHumidity()
        {
            ISession session = SessionManager.GetSession();
            List<AirHumidityByDay> humidity = new List<AirHumidityByDay>();


            if (session == null)
                return null;

            var HumidityData = session.Execute("select * from \"air_humidity_by_day\"");


            foreach (var hd in HumidityData)
            {
                AirHumidityByDay h = new AirHumidityByDay();
                h.cityID = hd["city_id"] != null ? hd["city_id"].ToString() : string.Empty;
                h.date = hd["date"] != null ? DateTime.Parse(hd["date"].ToString()) : DateTime.Parse(string.Empty);
                h.eventTime = hd["event_time"] != null ? DateTime.Parse(hd["event_time"].ToString()) : DateTime.Parse(string.Empty);
                h.airHumidity = hd["air_humidity"] != null ? Decimal.Parse(hd["air_humidity"].ToString()) : Decimal.Parse(string.Empty);
                humidity.Add(h);
            }
            return humidity;
        }

        public static void AddHumidity(string city_id, string date, string eventTime, string humidity)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
            try
            {
                RowSet HumidityData = session.Execute("insert into \"air_humidity_by_day\" (\"city_id\", \"date\", \"event_time\", \"air_humidity\")  values ('" + city_id + "' , '" + date + "' , '" + eventTime + "' , '" + humidity + "')");
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void DeleteHumidityByEventTime(string city_id, string date, string eventTime)
        {
            ISession session = SessionManager.GetSession();
            AirHumidityByDay Humidity = new AirHumidityByDay();

            if (session == null)
                return;

            RowSet HumidityData = session.Execute("delete from \"air_humidity_by_day\" where \"city_id\" = '" + city_id + "' and \"date\" = '" + date + "' and \"event_time\" = '" + eventTime + "'");
        }

        #endregion
    }
}

