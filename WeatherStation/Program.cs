﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStation
{
	class Program
	{
		static void Main(string[] args)
		{
            //			Console.WriteLine(DateTime.Now.ToShortTimeString());
            //			DataProvider.AddHumidity("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "23");
            //DataProvider.AddHumidity("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "23");
            //DataProvider.GetCityWeatherStations();
            //Console.WriteLine(DateTime.Now.Date);
            //Console.WriteLine(DateTime.Now.AddDays(1).ToShortDateString());
            //Console.WriteLine(DateTime.Now.AddDays(2).ToShortDateString());

            //Console.WriteLine(DateTime.Now.AddDays(1).AddHours(1).ToShortTimeString());
            //Console.WriteLine(DateTime.Now.AddDays(2).AddHours(2).ToShortTimeString());


            DataProvider.AddCityWeatherStation("Nis", "18000", "Srbija");
            DataProvider.AddCityWeatherStation("Bor", "19200", "Srbija");
            DataProvider.AddCityWeatherStation("Beograd", "11000", "Srbija");
            DataProvider.AddCityWeatherStation("Kragujevac", "34000", "Srbija");
            DataProvider.AddCityWeatherStation("Subotica", "24000", "Srbija");


            //pritisak
            //danas 
            DataProvider.AddPressure("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");



            //danas 
            DataProvider.AddPressure("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //danas 
            DataProvider.AddPressure("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");



            //danas 
            DataProvider.AddPressure("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");
            
            //nakosutra
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //danas 
            DataProvider.AddPressure("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddPressure("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //vlaznost
            //danas 
            DataProvider.AddHumidity("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");



            //danas 
            DataProvider.AddHumidity("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //danas 
            DataProvider.AddHumidity("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");



            //danas 
            DataProvider.AddHumidity("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //danas 
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddHumidity("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //temperatura
            //danas 
            DataProvider.AddTemperature("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Nis", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Nis", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");



            //danas 
            DataProvider.AddTemperature("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Beograd", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Beograd", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //danas 
            DataProvider.AddTemperature("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Subotica", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Subotica", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");



            //danas 
            DataProvider.AddTemperature("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Bor", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Bor", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //danas 
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //sutra
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(1).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //prekoustra
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(2).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //nakosutra
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(3).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


            //za 4 dana
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(4).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");

            //za 5 dana
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.ToShortTimeString(), "13");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(1).ToShortTimeString(), "14");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(2).ToShortTimeString(), "20");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(3).ToShortTimeString(), "22");
            DataProvider.AddTemperature("Kragujevac", DateTime.Now.AddDays(5).ToShortDateString(), DateTime.Now.AddHours(4).ToShortTimeString(), "21");


        }
    }
}
