﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStation.QueryEntities
{
    public class CityWeatherStation
    {
        public string cityID { get; set; }
        public string zipCode { get; set; }
        public string country { get; set; }
    }
}
