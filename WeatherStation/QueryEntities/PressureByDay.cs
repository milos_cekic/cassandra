﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStation.QueryEntities
{
    public class PressureByDay
    {
        public string cityID { get; set; }
        public DateTime date { get; set; }
        public DateTime eventTime { get; set; }
        public decimal pressure { get; set; }
    }
}
