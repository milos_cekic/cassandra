﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using WeatherStation;
using WeatherStation.QueryEntities;

namespace WStation.Controllers.APIControllers
{
    public class AirHumidityByDayController : ApiController
    {

        //[System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public AirHumidityByDay GetAirHumidityByEventTime(string cityId, string date, string eventTime)
        {
            AirHumidityByDay h = DataProvider.GetAirHumidityByEventTime(cityId, date, eventTime);
            return h;
        }


        // [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public IEnumerable<AirHumidityByDay> GetAirHumidityByDay(string cityId, string date)
        {
            IEnumerable<AirHumidityByDay> h = DataProvider.GetAirHumidityByDay(cityId, date);
            return h;
        }


        // [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public IEnumerable<AirHumidityByDay> GetHumidity()
        {
            IEnumerable<AirHumidityByDay> h = DataProvider.GetHumidity();
            return h;
        }

        // POST api/<controller>
        [System.Web.Mvc.HttpPost]
        public void Post([FromBody]AirHumidityByDay value)
        {
            AirHumidityByDay h = value;
            DataProvider.AddHumidity(h.cityID, h.date.ToString(), h.eventTime.ToString(), h.airHumidity.ToString());
            return;
        }

        // DELETE api/<controller>/5
        [System.Web.Mvc.HttpDelete]
        public void DeleteHumidityByEventTime(string cityId, string date, string eventTime)
        {
            DataProvider.DeleteHumidityByEventTime(cityId, date, eventTime);
            return;
        }
    }
}
