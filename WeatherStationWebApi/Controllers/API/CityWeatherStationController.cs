﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using WeatherStation;
using WeatherStation.QueryEntities;

namespace WStation.Controllers.APIControllers
{
    public class CityWeatherStationController : ApiController
    {
        // GET api/cityweatherstation
        //[System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public IEnumerable<CityWeatherStation> GetStations()
        {
            IEnumerable<CityWeatherStation> stations = DataProvider.GetCityWeatherStations();
            return stations;
        }

        // GET api/<controller>/5
        // [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public CityWeatherStation GetStation(string cityId)
        {
            CityWeatherStation station = DataProvider.GetCityWeatherStation(cityId);
            return station;
        }

        // POST api/<controller>
        [System.Web.Mvc.HttpPost]
        public void Post([FromBody]CityWeatherStation value)
        {
            CityWeatherStation station = value;
            DataProvider.AddCityWeatherStation(station.cityID, station.zipCode, station.country);
            return;
        }

        // DELETE api/<controller>/5
        [System.Web.Mvc.HttpDelete]
        public void DeleteCityWeatherStation(string cityId)
        {
            DataProvider.DeleteCityWeatherStation(cityId);
            return;
        }
    }
}