﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using WeatherStation;
using WeatherStation.QueryEntities;

namespace WStation.Controllers.APIControllers
{
    public class PressureByDayController : ApiController
    {


        //[System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public IEnumerable<PressureByDay> GetPressure()
        {
            IEnumerable<PressureByDay> pressure = DataProvider.GetPressure();
            return pressure;
        }


        // [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public IEnumerable<PressureByDay> GetPressureDay(string cityId, string date)
        {
            IEnumerable<PressureByDay> pressure = DataProvider.GetPressureByDay(cityId, date);
            return pressure;
        }


        // [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public PressureByDay GetPressureEventTime(string cityId, string date, string eventTime)
        {
            PressureByDay pressure = DataProvider.GetPressureByEventTime(cityId, date, eventTime);
            return pressure;
        }

        // POST api/<controller>
        [System.Web.Mvc.HttpPost]
        public void Post([FromBody]PressureByDay value)
        {
            PressureByDay pressure = value;
            DataProvider.AddPressure(pressure.cityID, pressure.date.ToString(), pressure.eventTime.ToString(), pressure.pressure.ToString());
            return;
        }

        // DELETE api/<controller>/5
        [System.Web.Mvc.HttpDelete]
        public void DeletePressureByEventTime(string cityId, string date, string eventTime)
        {
            DataProvider.DeletePressureByEventTime(cityId, date, eventTime);
            return;
        }

    }
}
