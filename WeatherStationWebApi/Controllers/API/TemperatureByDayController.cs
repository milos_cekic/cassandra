﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using WeatherStation;
using WeatherStation.QueryEntities;

namespace WStation.Controllers.APIControllers
{
    public class TemperatureByDayController : ApiController
    {
        //[System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public TemperatureByDay GetTemperatureByEventTime(string cityId, string date, string eventTime)
        {
            TemperatureByDay h = DataProvider.GetTemperatureByEventTime(cityId, date, eventTime);
            return h;
        }


        // [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public IEnumerable<TemperatureByDay> GetTemperatureByDay(string cityId, string date)
        {
            IEnumerable<TemperatureByDay> t = DataProvider.GetTemperatureByDay(cityId, date);
            return t;
        }


        // [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.HttpGet]
        public IEnumerable<TemperatureByDay> GetTemperature()
        {
            IEnumerable<TemperatureByDay> t = DataProvider.GetTemperature();
            return t;
        }

        // POST api/<controller>
        [System.Web.Mvc.HttpPost]
        public void Post([FromBody]TemperatureByDay value)
        {
            TemperatureByDay t = value;
            DataProvider.AddTemperature(t.cityID, t.date.ToString(), t.eventTime.ToString(), t.temperature.ToString());
            return;
        }

        // DELETE api/<controller>/5
        [System.Web.Mvc.HttpDelete]
        public void DeleteTemperatureByEventTime(string cityId, string date, string eventTime)
        {
            DataProvider.DeleteTemperatureByEventTime(cityId, date, eventTime);
            return;
        }
    }
}
